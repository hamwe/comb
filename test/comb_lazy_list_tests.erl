% Copyright 2018, Hampus Weddig <hampus.weddig@gmail.com>.
%
%    Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.

-module(comb_lazy_list_tests).

-include_lib("eunit/include/eunit.hrl").

suite_test() -> comb_gen_list_suite:suite(comb_lazy_list).

take(M,N,Xs) -> M:to_erlang_list(comb:take(M, N, Xs)).

repeat_test() ->
  M = comb_lazy_list,
  ?assert(take(M, 4, comb:repeat(M, 1)) =:= [1,1,1,1]),
  ok.
