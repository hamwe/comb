% Copyright 2018, Hampus Weddig <hampus.weddig@gmail.com>.
%
%    Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.

-module(comb_erlang_list_tests).

-include_lib("eunit/include/eunit.hrl").

from_erlang_list_test() ->
  M = comb_erlang_list,
  ?assert(comb:from_erlang_list(M, []) =:= []),
  ?assert(comb:from_erlang_list(M, [1,2,3]) =:= [1,2,3]),
  ok.

append_test() ->
  M = comb_erlang_list,
  ?assert(comb:append(M, [],[4,5]) =:= [4,5]),
  ?assert(comb:append(M, [1,2,3],[4,5]) =:= [1,2,3,4,5]),
  ok.

tail_test() ->
  M = comb_erlang_list,
  ?assert(comb:tail(M, [1,2,3]) =:= [2,3]),
  ok.

map_test() ->
  M = comb_erlang_list,
  ?assert(comb:map(M, fun(X) -> X+1 end, [1,2,3]) =:= [2,3,4]),
  ok.

reverse_test() ->
  M = comb_erlang_list,
  ?assert(comb:reverse(M, [1,2,3]) =:= [3,2,1]),
  ok.

intersperse_test() ->
  M = comb_erlang_list,
  ?assert(comb:intersperse(M, 0,[]) =:= []),
  ?assert(comb:intersperse(M, 0,[1]) =:= [1]),
  ?assert(comb:intersperse(M, 0,[1,2,3]) =:= [1,0,2,0,3]),
  ok.

foldr_test() ->
  M = comb_erlang_list,
  ?assert(comb:foldr(M, fun(X,Y) -> X+Y end, 0, []) =:= 0),
  ?assert(comb:foldr(M, fun(X,Y) -> X+Y end, 0, [1,2,3]) =:= 6),
  ?assert(comb:foldr(M, fun(X,Y) -> X-Y end, 0, [1,2,3]) =:= (1-(2-(3-0)))),
  ok.

foldl_test() ->
  M = comb_erlang_list,
  ?assert(comb:foldl(M, fun(X,Y) -> X+Y end, 0, []) =:= 0),
  ?assert(comb:foldl(M, fun(X,Y) -> X+Y end, 0, [1,2,3]) =:= 6),
  ?assert(comb:foldl(M, fun(X,Y) -> X-Y end, 0, [1,2,3]) =:= (((0-1)-2)-3)),
  ok.

concat_test() ->
  M = comb_erlang_list,
  ?assert(comb:concat(M, []) =:= []),
  ?assert(comb:concat(M, [[1,2],[3,4,5],[6]]) =:= [1,2,3,4,5,6]),
  ok.

concat_map_test() ->
  M = comb_erlang_list,
  ?assert(comb:concat_map(M, fun(X) -> [X,X+1] end, []) =:= []),
  ?assert(comb:concat_map(M, fun(X) -> [X,X+1] end, [1]) =:= [1,2]),
  ok.

filter_test() ->
  M = comb_erlang_list,
  ?assert(comb:filter(M, fun(X) -> X rem 3 == 0 end, []) =:= []),
  ?assert(comb:filter(M, fun(X) -> X rem 3 == 0 end, [1,2,3,4,5,6]) =:= [3,6]),
  ok.

zip_test() ->
  M = comb_erlang_list,
  ?assert(comb:zip(M, [],[]) =:= []),
  ?assert(comb:zip(M, [1],[]) =:= []),
  ?assert(comb:zip(M, [],[2]) =:= []),
  ?assert(comb:zip(M, [1],[2]) =:= [{1,2}]),
  ?assert(comb:zip(M, [1,2,3],[4,5,6]) =:= [{1,4},{2,5},{3,6}]),
  ok.

zipWit_test() ->
  M = comb_erlang_list,
  ?assert(comb:zip_with(M, fun(X,Y) -> X+Y end, [1,2,3], [4,5,6]) =:= [5,7,9]),
  ok.

take_test() ->
  M = comb_erlang_list,
  ?assert(comb:take(M, 10, []) =:= []),
  ?assert(comb:take(M, 2, [1,2,3,4]) =:= [1,2]),
  ok.
