% Copyright 2018, Hampus Weddig <hampus.weddig@gmail.com>.
%
%    Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.

-module(comb_list_tests).

-include_lib("eunit/include/eunit.hrl").

suite_test() -> comb_gen_list_suite:suite(comb_list).

take(M,N,Xs) -> M:to_erlang_list(comb:take(M, N, Xs)).

tmp_test() ->
  M = comb_list,
  ?assert(take(M, 4, [1,1,1,1,1,1,1]) =:= [1,1,1,1]),
  ?assert(M:to_erlang_list(comb:append(M, [1,2,3], comb:from_erlang_list(comb_iterator, [5,6,7]))) =:= [1,2,3,5,6,7]),
  ?assert(M:to_erlang_list(comb:append(M, comb:from_erlang_list(comb_iterator, [5,6,7]), [1,2,3])) =:= [5,6,7,1,2,3]),
  ok.

ones(M) -> M:fix(fun(Ones) -> M:cons(1, Ones) end).
naturals(M) -> M:fix(fun(Naturals,N) -> M:cons(N, Naturals(N+1)) end, 0).
fibs(M) -> M:fix(fun(Fibs,X,Y) -> M:cons(X, Fibs(Y,X+Y)) end, 0, 1).
fibs2() -> comb_list:fix(fun(Fibs,X,Y) -> [X | Fibs(Y,X+Y)] end, 0, 1).

fix_test() ->
  M = comb_list,
  ?assert(take(M, 5, ones(M)) =:= [1,1,1,1,1]),
  ?assert(take(M, 10, naturals(M)) =:= [0,1,2,3,4,5,6,7,8,9]),
  ?assert(take(M, 10, fibs(M)) =:= [0,1,1,2,3,5,8,13,21,34]),
  ?assert(take(M, 10, fibs2()) =:= [0,1,1,2,3,5,8,13,21,34]),
  ok.
