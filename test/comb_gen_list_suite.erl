% Copyright 2018, Hampus Weddig <hampus.weddig@gmail.com>.
%
%    Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.

-module(comb_gen_list_suite).

-include_lib("eunit/include/eunit.hrl").

-export([suite/1]).

suite(M) ->
  from_erlang_list_test(M),
  append_test(M),
  tail_test(M),
  map_test(M),
  reverse_test(M),
  intersperse_test(M),
  concat_test(M),
  concat_map_test(M),
  filter_test(M),
  zip_test(M),
  zip_with_test(M),
  take_test(M),
  ok.

up_down(M,F,Xs) -> M:to_erlang_list(F(comb:from_erlang_list(M,Xs))).
up_down(M,F,Xs,Ys) -> M:to_erlang_list(F(comb:from_erlang_list(M,Xs), comb:from_erlang_list(M,Ys))).
ups_down(M,F,Xss) -> M:to_erlang_list(F(ups(M, Xss))).
ups(M,Xss) ->
  comb:map(M, fun(Xs) -> comb:from_erlang_list(M,Xs) end, comb:from_erlang_list(M,Xss)).

from_erlang_list_test(M) ->
  ?assert(M:to_erlang_list(comb:from_erlang_list(M,[])) =:= []),
  ?assert(M:to_erlang_list(comb:from_erlang_list(M,[1,2,3])) =:= [1,2,3]),
  ok.

append_test(M) ->
  Append = fun(Xs,Ys) -> comb:append(M,Xs,Ys) end,
  ?assert(up_down(M, Append, [], [4,5]) =:= [4,5]),
  ?assert(up_down(M, Append, [1,2,3], [4,5]) =:= [1,2,3,4,5]),
  ok.

tail_test(M) ->
  Tail = fun(Xs) -> comb:tail(M,Xs) end,
  ?assert(up_down(M,Tail,[1,2,3]) =:= [2,3]),
  ok.

map_test(M) ->
  Map = fun(Xs) -> comb:map(M,fun(X) -> X+1 end, Xs) end,
  ?assert(up_down(M,Map,[1,2,3]) =:= [2,3,4]),
  ok.

reverse_test(M) ->
  Reverse = fun(Xs) -> comb:reverse(M,Xs) end,
  ?assert(up_down(M,Reverse,[1,2,3]) =:= [3,2,1]),
  ok.

intersperse_test(M) ->
  Intersperse = fun(Xs) -> comb:intersperse(M,0,Xs) end,
  ?assert(up_down(M,Intersperse,[]) =:= []),
  ?assert(up_down(M,Intersperse,[1]) =:= [1]),
  ?assert(up_down(M,Intersperse,[1,2,3]) =:= [1,0,2,0,3]),
  ok.

concat_test(M) ->
  Concat = fun(Xss) -> comb:concat(M,Xss) end,
  ?assert(ups_down(M,Concat,[]) =:= []),
  ?assert(ups_down(M,Concat,[[1,2],[3,4,5],[6]]) =:= [1,2,3,4,5,6]),
  ok.

concat_map_test(M) ->
  F = fun(X) -> comb:from_erlang_list(M, [X,X+1]) end,
  ConcatMap = fun(Xs) -> comb:concat_map(M, F, Xs) end,
  ?assert(up_down(M, ConcatMap, []) =:= []),
  ?assert(up_down(M, ConcatMap, [1]) =:= [1,2]),
  ?assert(up_down(M, ConcatMap, [1,2,3]) =:= [1,2,2,3,3,4]),
  ok.

filter_test(M) ->
  Filter = fun(Xs) -> comb:filter(M, fun(X) -> X rem 3 == 0 end, Xs) end,
  ?assert(up_down(M, Filter, []) =:= []),
  ?assert(up_down(M, Filter, [1,2,3,4,5,6]) =:= [3,6]),
  ok.

zip_test(M) ->
  Zip = fun(Xs,Ys) -> comb:zip(M,Xs,Ys) end,
  ?assert(up_down(M,Zip,[],[]) =:= []),
  ?assert(up_down(M,Zip,[1],[]) =:= []),
  ?assert(up_down(M,Zip,[],[2]) =:= []),
  ?assert(up_down(M,Zip,[1],[2]) =:= [{1,2}]),
  ?assert(up_down(M,Zip,[1,2,3],[4,5,6]) =:= [{1,4},{2,5},{3,6}]),
  ok.

zip_with_test(M) ->
  ZipWith = fun(Xs,Ys) -> comb:zip_with(M,fun(X,Y) -> X+Y end,Xs,Ys) end,
  ?assert(up_down(M, ZipWith, [1,2,3], [4,5,6]) =:= [5,7,9]),
  ok.

take_test(M) ->
  Take = fun(N) -> fun(Xs) -> comb:take(M,N,Xs) end end,
  ?assert(up_down(M, Take(10), []) =:= []),
  ?assert(up_down(M, Take(2), [1,2,3,4]) =:= [1,2]),
  ok.
