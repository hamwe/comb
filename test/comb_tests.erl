% Copyright 2018, Hampus Weddig <hampus.weddig@gmail.com>.
%
%    Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.

-module(comb_tests).

-include_lib("eunit/include/eunit.hrl").

inc(X) -> X+1.
neg(X) -> -X.
double(X) -> X*X.

compose_test() ->
  ?assert((comb:compose(fun inc/1, fun double/1))(2) =:= 5),
  ?assert((comb:compose([]))(2) =:= 2),
  ?assert((comb:compose([fun inc/1]))(2) =:= 3),
  ?assert((comb:compose([fun inc/1, fun neg/1]))(2) =:= -1),
  ?assert((comb:compose([fun inc/1, fun neg/1, fun double/1]))(2) =:= -3),
  ok.

apply_test() ->
  ?assert(comb:apply(fun inc/1, 3) =:= 4),
  ?assert(comb:apply([], 3) =:= 3),
  ?assert(comb:apply([fun inc/1], 3) =:= 4),
  ?assert(comb:apply([fun inc/1, fun neg/1, fun double/1], 3) =:= -8),
  ok.

pipe_test() ->
  ?assert(comb:pipe(4, fun inc/1) =:= 5),
  ?assert(comb:pipe(4, []) =:= 4),
  ?assert(comb:pipe(4, [fun inc/1]) =:= 5),
  ?assert(comb:pipe(4, [fun inc/1, fun neg/1, fun double/1]) =:= 25),
  ok.

example_test() ->
  M = comb_erlang_list,
  Result = comb:pipe([1,2,3,4,5,6,7,8,9,10], [
    comb:map(M, fun inc/1),
    comb:filter(M, fun(X) -> X rem 3 /= 0 end),
    comb:map(M, fun double/1),
    comb:take(M, 5)]),
  ?assert(Result =:= [4,16,25,49,64]).

append_test() ->
  ?assert([1,2,3,4] =:= comb:tel(comb:append([1,2],[3,4]))).

example_2_test() ->
  Result = comb:tel(comb:pipe([1,2,3,4,5,6,7,8,9,10], [
    comb:map(fun inc/1),
    comb:filter(fun(X) -> X rem 3 /= 0 end),
    comb:map(fun double/1),
    comb:take(5)])),
  ?assert(Result =:= [4,16,25,49,64]).

naturals_test() ->
  Result = comb:tel(comb:take(5, comb:naturals(10))),
  ?assert(Result =:= [10,11,12,13,14]).
