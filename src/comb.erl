% Copyright 2018, Hampus Weddig <hampus.weddig@gmail.com>.
%
%    Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.

-module(comb).

-export([
    id/1,
    compose/2,
    compose/1,
    compose/0,
    apply/2,
    pipe/2,
    pipe/1
    ]).

-export([
  from_erlang_list/2,
  fel/2,
  to_erlang_list/1,
  to_erlang_list/2,
  tel/0,
  tel/1,
  tel/2,
  fix/1,
  fix/2,
  fix/3,
  fix/4,
  append/2,
  append/3,
  tail/2,
  tail/1,
  tail/0,
  map/3,
  map/2,
  map/1,
  reverse/2,
  reverse/1,
  reverse/0,
  intersperse/3,
  intersperse/2,
  foldr/4,
  foldr/3,
  foldr/2,
  foldl/4,
  foldl/3,
  foldl/2,
  concat/2,
  concat/1,
  concat/0,
  concat_map/3,
  concat_map/2,
  concat_map/1,
  filter/3,
  filter/2,
  filter/1,
  zip/3,
  zip/2,
  zip_with/4,
  zip_with/3,
  iterate/3,
  iterate/2,
  repeat/2,
  repeat/1,
  cycle/2,
  cycle/1,
  take/3,
  take/2,
  take/1,
  naturals/0,
  naturals/1,
  naturals/2
]).

-define(DEFAULT_LIST, comb_list).

id(X) -> X.

compose(F,G) -> fun(X) -> F(G(X)) end.

compose(Xs) -> (compose())(Xs).
compose() -> foldr(comb_erlang_list, fun compose/2, fun id/1).

apply(F,X) when is_function(F) -> F(X);
apply(Fs,X) when is_list(Fs) -> foldr(comb_erlang_list, fun apply/2, X, Fs).

pipe(X,F) when is_function(F) -> F(X);
pipe(X,Fs) when is_list(Fs) -> (pipe(X))(Fs).
pipe(X) -> foldl(comb_erlang_list, fun pipe/2, X).

%% List
% uncons(M,Nil,Cons,nil(M)) = nil(M)
% uncons(M,Nil,Cons,cons(X,Xs)) = Cons(M,X,Xs)
% Xs = nil(M) | exist X Xs2 (Xs = cons(M,X,Xs2))
nil(M) -> M:nil().
cons(M,X,Xs) -> M:cons(X,Xs).
uncons(M,Nil,Cons,Xs) -> M:uncons(Nil,Cons,Xs).

uncons(M,Nil,Cons) -> fun(Xs) -> uncons(M,Nil,Cons,Xs) end.

from_erlang_list(M,[]) -> nil(M);
from_erlang_list(M,[X|Xs]) -> cons(M,X, from_erlang_list(M,Xs)).

fel(M,Xs) -> from_erlang_list(M,Xs).

to_erlang_list(Xs) -> to_erlang_list(?DEFAULT_LIST, Xs).
to_erlang_list(M,Xs) -> M:to_erlang_list(Xs).

tel() -> fun(Xs) -> tel(Xs) end.
tel(Xs) -> to_erlang_list(Xs).
tel(M,Xs) -> to_erlang_list(M,Xs).

fix(F) -> fix(?DEFAULT_LIST,F).
fix(M,F) when is_atom(M) -> M:fix(F);
fix(F,X) when is_function(F) -> fix(?DEFAULT_LIST, F,X).
fix(M,F,X) when is_atom(M) -> M:fix(F,X);
fix(F,X1,X2) when is_function(F) -> fix(?DEFAULT_LIST, F, X1, X2).
fix(M,F,X1,X2) -> M:fix(F,X1,X2).

append(Xs,Ys) -> append(?DEFAULT_LIST, Xs, Ys).
append(M,Xs,Ys) ->
  uncons(M,Ys, fun(X,Xs2) -> cons(M,X, append(M,Xs2,Ys)) end, Xs).

tail(M,Xs) -> (tail(M))(Xs).
tail(M) when is_atom(M) -> uncons(M,undefined, fun(_X,Xs) -> Xs end);
tail(Xs) when is_list(Xs) or is_function(Xs) -> tail(?DEFAULT_LIST, Xs).
tail() -> tail(?DEFAULT_LIST).

map(M,F,Xs) -> (map(M,F))(Xs).
map(M,F) when is_atom(M) ->
  uncons(M,nil(M), fun(X,Xs) -> cons(M,F(X), map(M,F,Xs)) end);
map(F,Xs) when is_function(F) -> map(?DEFAULT_LIST, F, Xs).
map(F) -> map(?DEFAULT_LIST, F).

reverse(M,Xs) -> reverse2(M,nil(M),Xs).
reverse(M) when is_atom(M) -> fun(Xs) -> reverse(M,Xs) end;
reverse(Xs) when is_list(Xs) or is_function(Xs) ->
  reverse(?DEFAULT_LIST, Xs).
reverse() -> reverse(?DEFAULT_LIST).

reverse2(M,Xs,Ys) -> uncons(M,Xs, fun(X,Xs2) ->
  reverse2(M,cons(M,X,Xs),Xs2) end, Ys).

intersperse(M,E,Xs) -> (intersperse(M,E))(Xs).
intersperse(M,E) -> uncons(M,nil(M), fun(X,Xs) ->
  cons(M,X,intersperse2(M,E,Xs)) end).

intersperse2(M,E,Xs) -> (intersperse2(M,E))(Xs).
intersperse2(M,E) -> uncons(M,nil(M), fun(X,Xs) ->
  cons(M,E,cons(M,X,intersperse2(M,E,Xs))) end).

foldr(M,Op,E,Xs) -> (foldr(M,Op,E))(Xs).
foldr(M,Op,E) when is_atom(M) ->
  uncons(M,E, fun(X,Xs) -> Op(X, foldr(M,Op,E,Xs)) end);
foldr(Op,E,Xs) when is_function(Op) -> foldr(?DEFAULT_LIST, Op, E, Xs).
foldr(Op, E) -> foldr(?DEFAULT_LIST, Op, E).

foldl(M,Op,E,Xs) -> (foldl(M,Op,E))(Xs).
foldl(M,Op,E) when is_atom(M) ->
  uncons(M,E, fun(X,Xs) -> foldl(M,Op,Op(E,X),Xs) end);
foldl(Op,E,Xs) when is_function(Op) -> foldl(?DEFAULT_LIST, Op, E, Xs).
foldl(Op,E) -> foldl(?DEFAULT_LIST, Op, E).

concat(M,Xs) -> (concat(M))(Xs).
concat(M) when is_atom(M) ->
  uncons(M,nil(M), fun(Xs,Xss) -> append(M,Xs, concat(M,Xss)) end);
concat(Xs) when is_list(Xs) or is_function(Xs) -> concat(?DEFAULT_LIST, Xs).
concat() -> concat(?DEFAULT_LIST).

concat_map(M,F,Xs) -> concat(M,map(M,F, Xs)).
concat_map(M,F) when is_atom(M) -> fun(Xs) -> concat_map(M,F,Xs) end;
concat_map(F,Xs) when is_function(F) -> concat_map(?DEFAULT_LIST, F, Xs).
concat_map(F) -> concat_map(?DEFAULT_LIST, F).

filter(M,P,Xs) -> (filter(M,P))(Xs).
filter(M,P) when is_atom(M) ->
  Cons = fun(X,Xs) -> case P(X) of true -> cons(M,X, filter(M,P,Xs));
                                   false -> filter(M,P,Xs)
                      end
         end,
  uncons(M,nil(M), Cons);
filter(P,Xs) when is_function(P) -> filter(?DEFAULT_LIST, P, Xs).
filter(P) -> filter(?DEFAULT_LIST, P).

zip(M,Xs,Ys) ->
  uncons(
    M,
    nil(M),
    fun(X,Xs2) ->
      uncons(
        M,
        nil(M),
        fun(Y,Ys2) -> cons(M,{X,Y}, zip(M,Xs2,Ys2)) end,
        Ys
      )
    end,
    Xs
  ).
zip(Xs,Ys) -> zip(?DEFAULT_LIST, Xs, Ys).

zip_with(M,F,Xs,Ys) -> map(M,fun({X,Y}) -> F(X,Y) end, zip(M,Xs,Ys)).
zip_with(F,Xs,Ys) -> zip_with(?DEFAULT_LIST, F, Xs, Ys).

iterate(M,F,X) -> M:fix(fun(Iterate, Y) -> M:cons(Y, Iterate(F(Y))) end, X).
iterate(F,X) -> iterate(?DEFAULT_LIST, F, X).

repeat(M,X) -> M:fix(fun(Repeat) -> M:cons(X, Repeat) end).
repeat(X) -> repeat(?DEFAULT_LIST, X).

cycle(M,Xs) -> M:fix(fun(Cycle) -> append(M, Xs, Cycle) end).
cycle(Xs) -> cycle(?DEFAULT_LIST, Xs).

take(M,0,_Xs) -> nil(M);
take(M,N,Xs) -> uncons(M,nil(M), fun(X,Xs2) -> cons(M,X, take(M,N-1, Xs2)) end, Xs).
take(M,N) when is_atom(M) -> fun(Xs) -> take(M,N,Xs) end;
take(N,Xs) when is_integer(N) -> take(?DEFAULT_LIST, N, Xs).
take(N) -> take(?DEFAULT_LIST, N).

naturals(M,N0) ->
  comb:fix(M, fun(Naturals,N) -> M:cons(N, Naturals(N+1)) end, N0).
naturals(M) when is_atom(M) -> naturals(M,0);
naturals(N) when is_integer(N) -> naturals(?DEFAULT_LIST, N).
naturals() -> naturals(?DEFAULT_LIST).
