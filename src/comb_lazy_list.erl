% Copyright 2018, Hampus Weddig <hampus.weddig@gmail.com>.
%
%    Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.

-module(comb_lazy_list).

% Experimental

-export([nil/0, cons/2, uncons/3, to_erlang_list/1, fix/1]).

-type state(X,S) :: fun((S) -> {X,S}).
-type lstate(X) :: state(X,{integer(), map()}).
-type lazylist(X) :: lstate(nil | {cons, X, lazylist(X)}).

-spec unit(X) -> fun((S) -> {X,S}).
unit(X) -> fun(S) -> {X,S} end.

-spec bind(state(X,S), fun((X) -> state(Y,S))) -> state(Y,S).
bind(M,F) -> fun(S1) -> {X,S2} = M(S1), (F(X))(S2) end.

-spec eval(state(X,S),S) -> X.
eval(M,S) -> {X,_} = M(S), X.

-spec eval(lstate(X)) -> X.
eval(M) -> eval(M,{0,#{}}).

-spec new(lstate(any())) -> lstate(integer()).
new(F) -> fun({I,M}) -> {I,{I+1,maps:put(I,{left, F},M)}} end.

-spec read(integer()) -> lstate(any()).
read(V) -> fun({I,M}) ->
    case maps:get(V,M,undefined) of
      {left, F} ->
        {X,{I2,M2}} = F({I,M}),
        {X,{I2, maps:put(V,{right, X},M2)}};
      {right, X} ->
        {X,{I,M}};
      undefined ->
        undefined
    end
  end.

-spec write(integer(), lstate(any())) -> lstate({}).
write(V,F) -> fun({I,M}) -> {{}, {I, maps:put(V,{left, F},M)}} end.

-spec nil() -> lazylist(any()).
nil() ->
  new(unit(nil)).

-spec cons(X,lazylist(X)) -> lazylist(X).
cons(X,Xs) ->
  new(bind(Xs, fun(XsR) -> unit({cons, X, XsR}) end)).

-spec uncons(lazylist(Y), fun((X,lazylist(X)) -> lazylist(Y)), lazylist(X)) -> lazylist(Y).
uncons(Nil,Cons,Xs) ->
  new(bind(Xs, fun(XsR) ->
    bind(read(XsR), fun(XsV) ->
      case XsV of
        nil -> read_lazylist(Nil);
        {cons, X, XsR2} -> read_lazylist(Cons(X, unit(XsR2)))
      end
    end)
  end)).

-spec to_erlang_list(lazylist(X)) -> list(X).
to_erlang_list(Xs) -> eval(bind(Xs, fun to_erlang_list_m/1)).

to_erlang_list_m(Xs) ->
  bind(read(Xs), fun(XsV) ->
    case XsV of
      nil -> unit([]);
      {cons, X, Xs2} ->
        bind(to_erlang_list_m(Xs2), fun(Xs3) ->
          unit([X|Xs3])
        end)
    end
  end).

read_lazylist(Xs) -> bind(Xs, fun(V) -> read(V) end).

-spec fix(fun((lazylist(X)) -> lazylist(X))) -> lazylist(X).
fix(F) ->
  bind(new(unit(undefined)), fun(Xs1) ->
    bind(read_lazylist(F(unit(Xs1))), fun(Xs2) ->
      bind(write(Xs1, unit(Xs2)), fun(_) ->
        unit(Xs1) end) end) end).
