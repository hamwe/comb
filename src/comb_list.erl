% Copyright 2018, Hampus Weddig <hampus.weddig@gmail.com>.
%
%    Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.

-module(comb_list).

-export([nil/0, cons/2, uncons/3]).
-export([to_erlang_list/1, fix/1, fix/2, fix/3]).
-export([iterator/1, foreach/1, foreach/2]).

-export([
  from_stdin/0,
  lines_from_stdin/0,
  lines_from_stdin/1,
  from_device/1,
  lines_from_device/1,
  lines_from_device/2,
  to_stdout/0,
  to_stdout/1,
  to_device/1,
  to_device/2
]).

% Not exactly true
-type comb_list(A) :: list(A) | fun(() -> nil | {cons, A, comb_list(A)}).

-spec nil() -> comb_list(any()).
nil() -> fun() -> nil end.

-spec cons(A, comb_list(A)) -> comb_list(A).
cons(X,Xs) -> fun() -> {cons, X, Xs} end.

-spec
  uncons(
    comb_list(B),
    fun((A, comb_list(A)) -> comb_list(B)),
    comb_list(A))
  -> comb_list(B).
uncons(Nil,Cons,List) ->
  fun() ->
    case aux(List) of
      nil -> aux(Nil);
      [] -> aux(Nil);
      {cons,X,Xs} -> aux(Cons(X,Xs));
      [X|Xs] -> aux(Cons(X,Xs))
    end
  end.

aux(Xs) when is_function(Xs) -> Xs();
aux(Xs) -> Xs.

-spec to_erlang_list(comb_list(A)) -> list(A).
to_erlang_list(Xs) ->
  case aux(Xs) of
    nil -> [];
    {cons, X, Xs2} -> [X|to_erlang_list(Xs2)];
    [] -> [];
    [X|Xs2] -> [X|to_erlang_list(Xs2)]
  end.

-spec fix(fun((comb_list(A)) -> comb_list(A))) -> comb_list(A).
fix(F) -> F(fun() -> aux(fix(F)) end).

-spec
  fix(
    fun((fun((A) -> comb_list(B)), A) -> comb_list(B)),
    A)
  -> comb_list(B).
fix(F,X) -> F(fun(Y) -> fun() -> aux(fix(F,Y)) end end, X).

-spec
  fix(
    fun((fun((A, B) -> comb_list(C)), A, B) -> comb_list(C)),
    A,
    B)
  -> comb_list(C).
fix(F,X1,X2) -> F(fun(Y1,Y2) -> fun() -> aux(fix(F,Y1,Y2)) end end, X1, X2).

-spec iterator(fun(() -> nil | {cons, A, comb_list(A)})) -> comb_list(A).
iterator(F) -> F.

-spec foreach(fun((A) -> any())) -> fun((comb_list(A)) -> ok).
foreach(F) -> fun(Xs) -> foreach(F, Xs) end.

-spec foreach(fun((A) -> any()), comb_list(A)) -> ok.
foreach(F,Xs) ->
  case aux(Xs) of
    [] -> ok;
    [X|Xs2] -> F(X), foreach(F, Xs2);
    nil -> ok;
    {cons, X, Xs2} -> F(X), foreach(F, Xs2)
    end.

% Some examples

-spec from_stdin() -> comb_list(char()).
from_stdin() -> comb:concat(lines_from_stdin()).

-spec lines_from_stdin() -> comb_list(string()).
lines_from_stdin() -> lines_from_stdin("").

-spec lines_from_stdin(string()) -> comb_list(string()).
lines_from_stdin(Prompt) -> lines_from_device(standard_io, Prompt).

-spec from_device(io:device()) -> comb_list(char()).
from_device(IoDevice) -> comb:concat(lines_from_device(IoDevice)).

-spec lines_from_device(io:device()) -> comb_list(string()).
lines_from_device(IoDevice) -> lines_from_device(IoDevice, "").

-spec lines_from_device(io:device(), string()) -> comb_list(string()).
lines_from_device(IoDevice, Prompt) -> iterator(fun() ->
    case io:get_line(IoDevice, Prompt) of
      Cs when is_list(Cs) -> {cons, Cs, lines_from_device(IoDevice, Prompt)};
      _ -> nil
    end
  end).

-spec to_stdout() -> fun((comb_list(char())) -> ok).
to_stdout() -> fun(Cs) -> to_stdout(Cs) end.

-spec to_stdout(comb_list(char())) -> ok.
to_stdout(Cs) -> foreach(fun(C) -> io:put_chars([C]) end, Cs).

-spec to_device(io:device()) -> fun((comb_list(char())) -> ok).
to_device(IoDevice) -> fun(Cs) -> to_device(IoDevice, Cs) end.

-spec to_device(io:device(), comb_list(char())) -> ok.
to_device(IoDevice, Cs) -> foreach(fun(C) -> io:put_chars(IoDevice, [C]) end, Cs).
