% Copyright 2018, Hampus Weddig <hampus.weddig@gmail.com>.
%
%    Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.

-module(comb_iterator).

-export([nil/0, cons/2, uncons/3]).
-export([fix/1, fix/2, to_erlang_list/1, lift/1]).

nil() -> fun() -> nil end.
cons(X,Xs) -> fun() -> {cons, X, Xs} end.
uncons(Nil,Cons,List) ->
  fun() ->
    case List() of
      nil -> Nil();
      {cons,X,Xs} -> (Cons(X,Xs))()
    end
  end.

to_erlang_list(Xs) ->
  case Xs() of
    nil -> [];
    {cons, X, Xs2} -> [X|to_erlang_list(Xs2)]
  end.

lift(F) -> fun() -> {cons, F(), lift(F)} end.

fix(F) -> F(fun() -> (fix(F))() end).

fix(F,X) -> F(fun(Y) -> fun() -> (fix(F,Y))() end end, X).
