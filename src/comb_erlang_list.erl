% Copyright 2018, Hampus Weddig <hampus.weddig@gmail.com>.
%
%    Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.

-module(comb_erlang_list).

-export([nil/0, cons/2, uncons/3]).
-export([fix/1, to_erlang_list/1]).

nil() -> [].
cons(X,Xs) -> [X|Xs].
uncons(Nil,_Cons,[]) -> Nil;
uncons(_Nil,Cons,[X|Xs]) -> Cons(X,Xs).

fix(F) -> F(fix(F)). % This obviously wont work
to_erlang_list(Xs) -> Xs.
