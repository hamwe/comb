% Copyright 2018, Hampus Weddig <hampus.weddig@gmail.com>.
%
%    Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.

-module(comb_list_examples).

-export([
  string_to_stdout/0,
  from_stdin_to_stdout/0,
  to_device_from_device/0,
  fibs/0
]).

% prints a string to stdout
string_to_stdout() ->
  comb_list:to_stdout("Some kind of string\n").

% Reads 10 characters from stdin and writes them to stdout
from_stdin_to_stdout() ->
  comb:pipe(comb_list:from_stdin(), [comb:take(10), comb_list:to_stdout()]).

% Writes content to a file, reads from the file and prints content to stdout
to_device_from_device() ->
  FileName = random_file_name(),
  {ok, WriteDevice} = file:open(FileName, [write]),
  Content = "Some kind of content written then read from file\n",
  comb:pipe(Content, [comb_list:to_device(WriteDevice)]),
  file:close(WriteDevice),
  {ok, ReadDevice} = file:open(FileName, [read]),
  comb:pipe(comb_list:from_device(ReadDevice), [comb_list:to_stdout()]),
  file:close(ReadDevice),
  file:delete(FileName).

random_file_name() -> "/tmp/" ++ io_lib:print(rand:uniform(100000000000000000)).

% use fib as an expensive computation
fib(0) -> 0;
fib(1) -> 1;
fib(N) -> fib(N-1) + fib(N-2).

% Example to show that for comb_list can work with regular erlang lists as input
% and that results are not computed until needed
fibs() ->
  Ns = lists:seq(1, 100),
  Fibs = comb:map(fun fib/1, Ns),
  FirstFibs = comb:take(10, Fibs),
  comb:to_erlang_list(FirstFibs).
