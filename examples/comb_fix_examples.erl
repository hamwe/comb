% Copyright 2018, Hampus Weddig <hampus.weddig@gmail.com>.
%
%    Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.

-module(comb_fix_examples).

-export([ones_iterator/1, fibs_iterator/1, fibs_lazy_list/1]).
-export([naturals_hybrid_list/1, fibs_v2_hybrid_list/1]).

% infinite list of ones. Will not work with regular Erlang list
ones(M) -> M:fix(fun(Ones) -> M:cons(1, Ones) end).

% Will work with iterator
ones_iterator(N) ->
  M = comb_iterator,
  take(M,N,ones(M)).

% recursive infinte list of the fibonacci numbers
% fibs = 0:1:zip_with (+) fibs (tail fibs)
fibs(M) ->
  Add = fun(X,Y) -> X+Y end,
  M:fix(fun(Fibs) ->
    M:cons(0, M:cons(1, comb:zip_with(M, Add, Fibs, comb:tail(M, Fibs))))
  end).

% This will work for smaller N with the comb_iterator,
% but execution time blows up with larger N
fibs_iterator(N) ->
  M = comb_iterator,
  take(M,N,fibs(M)).

% The fibonacci sequence is efficient with the lazy_list implemenation
fibs_lazy_list(N) ->
  M = comb_lazy_list,
  take(M,N,fibs(M)).

take(M,N,Xs) -> M:to_erlang_list(comb:take(M,N,Xs)).

naturals(M) -> M:fix(fun(Naturals,N) -> M:cons(N, Naturals(N+1)) end, 0).
fibs_v2(M) -> M:fix(fun(Fibs,X,Y) -> M:cons(X, Fibs(Y, X+Y)) end, 0, 1).

naturals_hybrid_list(N) ->
  M = comb_list,
  take(M, N, naturals(M)).

fibs_v2_hybrid_list(N) ->
  M = comb_list,
  take(M, N, fibs_v2(M)).
